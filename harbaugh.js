$(function () {
    $('#container1').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: '2015 Home Attendance'
        },
        xAxis: {
            categories: [
                'Oregon State',
                'UNLV',
                'BYU',
                'Northwestern',
                'Michigan State',
                'Rutgers',
                'Ohio State'
            ],
            crosshair: true
        },
        yAxis: {
            min: 100000,
            title: {
                text: 'Attendance'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} people</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Attendance',
            data: [109651, 108683, 108940, 110452, 111740, 109879, 111829]
        }],
        credits: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        legend: {
            enabled: false
        }
    });

    $('#container2').highcharts({
        title: {
            text: 'AP Poll Ranking Over the Season',
            x: -20 //center
        },
        xAxis: {
            categories: ['Preseason', 'Week 2', 'Week 3', 'Week 4', 'Week 5', 'Week 6', 'Week 7',
             'Week 8', 'Week 9', 'Week 10', 'Week 11', 'Week 12', 'Week 13', 'Week 14', 'Week 15', 'Final']
        },
        yAxis: {
            title: {
                text: 'Ranking'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'AP Poll Ranking',
            data: [0, 0, 0, 0, 22, 18, 12, 15, 15, 16, 15, 14, 12, 19, 17, 12]
        }],
        exporting: {
            enabled: false
        },
        legend: {
            enabled: false
        }
    });
});